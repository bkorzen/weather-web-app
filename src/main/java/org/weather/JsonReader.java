package org.weather;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.aksingh.owmjapis.DailyForecast;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class JsonReader {

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);
        } finally {
            is.close();
        }
    }

    public static String getPhoto(String city) throws IOException, JSONException {
        String city2 = city.replaceAll("\\s", "%20");
        JSONObject json = readJsonFromUrl("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + city2 + "&key=AIzaSyC6RCO5zb4y_DwC-EzTGwywemWVA-QFo2s");
        if (json.get("status").equals("ZERO_RESULTS")) {
            return null;
        } else {
            String results = json.getJSONArray("results").getJSONObject(0).getJSONArray("photos").getJSONObject(0).getString("photo_reference");
            return "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + results + "&key=AIzaSyC6RCO5zb4y_DwC-EzTGwywemWVA-QFo2s";
        }
    }

    public static void main(String[] args) throws IOException, JSONException {
        JSONObject json = readJsonFromUrl("https://api.openweathermap.org/data/2.5/forecast?zip=75016,FR&APPID=6e84a50c8727c52c32db1ac98aba36e5");
        System.out.println(json.toString());
        ObjectMapper objectMapper = new ObjectMapper();
        List<DailyForecast> forecasts = new ArrayList<DailyForecast>();
        forecasts = objectMapper.readValue(json.toString(), new TypeReference<List<DailyForecast>>() {
        });


    }
}