package org.weather;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Logger {
    private static Logger logger = null; //logger instance variable

    private Logger() {

    }
    public void logGetDataFromWebsite(String string){
        Date now = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("E yyyy/MM/dd hh:mm ");
        System.out.println(dateFormatter.format(now) + string);
    }

    //singleton logger, only one instance is allowed
    public static synchronized Logger getInstance(){
        if(logger == null)
            logger = new Logger();
        return logger;
    }

}
