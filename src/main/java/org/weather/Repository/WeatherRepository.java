package org.weather.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.weather.model.Weather;

@Repository("WeatherRepository")
public interface WeatherRepository extends JpaRepository<Weather,Integer> {

    Weather findByCityID(int id);

    @Transactional
    Long deleteByCityID(int id);
}
