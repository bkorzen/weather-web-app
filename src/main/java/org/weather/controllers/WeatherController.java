package org.weather.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.weather.Logger;
import org.weather.Service.WeatherService;
import org.weather.model.Weather;

import java.io.IOException;

@Controller
public class WeatherController {

    @Autowired
    WeatherService weatherService;
    private Weather weather;
    private Logger logger;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView doWeather(@RequestParam(value = "city", required = false, defaultValue = "Krakow")
                                          String city, ModelAndView model) throws IOException {
        logger = Logger.getInstance();
        String[] strings = null;
        //JsonReader.main(strings);

        weather = new Weather(city);

        if (weather.getCityURL() == null) {
            city = "Krakow";
            weather = new Weather(city);
        }

        logger.logGetDataFromWebsite("DATA FETCHING FOR " + city);

        if ((model = getWeather(weather, city, model)) != null) {
            weatherService.saveWeather(weather);
            model.setViewName("weather");
        } else {
            logger.logGetDataFromWebsite("UNSUCCESSFULL DATA FETCHING FOR " + city);
            model.setViewName("weather");
        }
        return model;
    }

    ModelAndView getWeather(Weather weather, String city, ModelAndView model) throws IOException {

        if (weather.getWeather() != null) {
            model.addObject("weatherIcon", weather.getIconID());
            model.addObject("nextWeatherIcon", weather.getNextIconID());
            model.addObject("nextNextWeatherIcon", weather.getNextNextIconID());
            model.addObject("cityName", weather.getCityName());
            model.addObject("cityPhoto", weather.getCityPhoto());
            model.addObject("description", weather.getDescription());
            model.addObject("temperature", weather.getTemperature());
            model.addObject("nextTemperature", weather.getNextTemperature());
            model.addObject("nextNextTemperature", weather.getNextNextTemperature());
            model.addObject("pressure", weather.getPressure());
            model.addObject("humidity", weather.getHumidity());
            model.addObject("wind", weather.getWind());
            model.addObject("textWeather", weather.toString());
            model.addObject("sunriseTime", weather.getSunriseTime());
            model.addObject("sunsetTime", weather.getSunsetTime());
            model.addObject("time", weather.getTime());
            model.addObject("nextNextDate", weather.getNextNextDate());
            model.addObject("successMessage", "Success message");
            Weather.HourlyForecastData hf = weather.getHourlyForecast();
            model.addObject("hourForecast", hf.hours);
            model.addObject("temperatureForecast", hf.temperatures);
            model.addObject("iconForecast", hf.icons);

            return model;
        } else {
            model.addObject("successMessage", "");
            return model;
        }
    }

}
