package org.weather.model;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.HourlyForecast;
import net.aksingh.owmjapis.OpenWeatherMap;
import org.json.JSONException;
import org.weather.JsonReader;
import org.weather.Service.*;

import javax.persistence.*;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "weather")
public class Weather {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cityID;
    @Column(name = "city_name")
    private String cityName;
    @Column(name = "cityURL", length = 65535, columnDefinition = "Text")
    private String cityURL;
    private String description;
    private String temperature;
    private String nextTemperature;
    private String nextNextTemperature;

    private String pressure;
    private String humidity;
    private String wind;

    private String iconID;
    private String nextIconID;
    private String nextNextIconID;
    private Date nextNextDate;
    private Date time;
    private Date sunsetTime;
    private Date sunriseTime;

    public Weather() {
    }

    public Weather(String cityName) throws JSONException, IOException {
        this.cityName = cityName;
        this.cityURL = JsonReader.getPhoto(cityName);
        getWeather();
    }

    public String getCityURL() {
        return cityURL;
    }

    public String getNextTemperature() {
        return nextTemperature;
    }

    public String getNextNextTemperature() {
        return nextNextTemperature;
    }

    public String getNextIconID() {
        return nextIconID;
    }

    public String getNextNextIconID() {
        return nextNextIconID;
    }

    public String getTime() {
        String split[] = time.toString().split(" ");
        String tmp = split[3];
        String split2[] = tmp.split(":");
        return split2[0] + ":" + split2[1] + " | " + split[1] + " " + split[2] + ", " + split[5];
    }

    public String getSunsetTime() {
        String split[] = sunsetTime.toString().split(" ");
        String tmp = split[3];
        String split2[] = tmp.split(":");
        return split2[0] + ":" + split2[1];
    }

    public String getSunriseTime() {
        String split[] = sunriseTime.toString().split(" ");
        String tmp = split[3];
        String split2[] = tmp.split(":");
        return split2[0] + ":" + split2[1];
    }

    public String getCityName() {
        return cityName;
    }

    public String getCityPhoto() {
        return cityURL;
    }

    public String getDescription() {
        return description;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getPressure() {
        return pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getWind() {
        return wind;
    }

    public String getIconID() {
        return iconID;
    }

    public HourlyForecastData getHourlyForecast() throws JSONException, IOException {
        OpenWeatherMap.Units units = OpenWeatherMap.Units.METRIC;
        OpenWeatherMap wm = new OpenWeatherMap(units, "204e54dfcda152e7756c216dad2dfe27"); // personal OWM API KEY

        CurrentWeather cw = wm.currentWeatherByCityName(cityName);

        if ((int) cw.getCityCode() == 0)
            return null;

        HourlyForecastData data = new HourlyForecastData();
        data.hours = new ArrayList<String>();
        data.temperatures = new ArrayList<String>();
        data.icons = new ArrayList<String>();

        try {
            HourlyForecast forecast = wm.hourlyForecastByCityName(cityName);
            DateFormat formatter = new SimpleDateFormat("HH:mm");

            for (int i = 0; i < forecast.getForecastCount(); i++) {

                String date = formatter.format(forecast.getForecastInstance(i).getDateTime());

                String temperature = String.format("%.1f", forecast.getForecastInstance(i).getMainInstance().getTemperature());

                String icon = "";
                if (forecast.getForecastInstance(i).hasWeatherInstance())
                    icon = forecast.getForecastInstance(i).getWeatherInstance(0).getWeatherIconName();

                data.hours.add(date);
                data.temperatures.add(temperature);
                data.icons.add(icon);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public String getNextNextDate() {
        String split[] = this.nextNextDate.toString().split(" ");
        return split[0];
    }

    // gets weather data from OWM
    public Weather getWeather() throws JSONException, IOException {
        if (this.cityURL == null){
            return null;
        }
        OpenWeatherMap.Units units = OpenWeatherMap.Units.METRIC;
        OpenWeatherMap wm = new OpenWeatherMap(units, "6e84a50c8727c52c32db1ac98aba36e5"); // personal OWM API KEY

        CurrentWeather cw;
        float tempC;

        cw = wm.currentWeatherByCityName(cityName);

        if ((int) cw.getCityCode() == 0)
            return null;

        this.cityID = (int) cw.getCityCode();
        this.description = cw.getWeatherInstance(0).getWeatherDescription();

        tempC = cw.getMainInstance().getTemperature();
        this.temperature = String.format("%.1f", tempC);
        this.pressure = String.valueOf(cw.getMainInstance().getPressure());
        this.humidity = String.valueOf(cw.getMainInstance().getHumidity());
        float kmph = (cw.getWindInstance().getWindSpeed()) * 1.609F;
        this.wind = String.format("%.1f", kmph);
        this.iconID = cw.getWeatherInstance(0).getWeatherIconName();
        this.sunriseTime = cw.getSysInstance().getSunriseTime();
        this.sunsetTime = cw.getSysInstance().getSunsetTime();
        this.time = cw.getDateTime();

        try {
            HourlyForecast forecast = wm.hourlyForecastByCityName(cityName);
            DateFormat formatter = new SimpleDateFormat("HH:mm");

            int i = 0;
            int numEl = 0;

            while (numEl < 2 && i < forecast.getForecastCount()) {
                //sformatowanie daty z każdej iteracji do postaci samej godziny ( HH:mm )
                String date2 = formatter.format(forecast.getForecastInstance(i).getDateTime());

                //pobranie temperatury i ikony na następny dzień z godziny 13:00
                if (date2.compareTo("14:00") == 0 || date2.compareTo("13:00") == 0 || date2.compareTo("15:00") == 0) {
                    if (numEl == 0) {
                        nextTemperature = String.format("%.1f", forecast.getForecastInstance(i).getMainInstance().getTemperature());
                        if (forecast.getForecastInstance(i).hasWeatherInstance())
                            nextIconID = forecast.getForecastInstance(i).getWeatherInstance(0).getWeatherIconName();
                        numEl++;
                    } else if (numEl == 1) {
                        nextNextTemperature = String.format("%.1f", forecast.getForecastInstance(i).getMainInstance().getTemperature());
                        if (forecast.getForecastInstance(i).hasWeatherInstance())
                            nextNextIconID = forecast.getForecastInstance(i).getWeatherInstance(0).getWeatherIconName();
                        nextNextDate = forecast.getForecastInstance(i).getDateTime();
                        numEl++;
                    }
                }
                i++;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return this;
    }

//    @Override
//    public String toString() {
//        return
//                "ID: " + cityID + "\n" +
//                        "City Name: " + cityName + "\n" +
//                        "Description: " + description + "\n" +
//                        "Temperature: " + temperature + " C" + "\n" +
//                        "Pressure: " + pressure + " mbar" + "\n" +
//                        "Humidity: " + humidity + " %" + "\n" +
//                        "Wind: " + wind + " kmph" + "\n" +
//                        "Sunrise Time: " + getSunriseTime() + "\n" +
//                        "Sunset Time:  " + getSunsetTime() + "\n" +
//                        "Icon Name: " + iconID;
//    }

    public class HourlyForecastData {
        public ArrayList<String> hours;
        public ArrayList<String> temperatures;
        public ArrayList<String> icons;
    }

}
