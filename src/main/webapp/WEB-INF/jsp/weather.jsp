<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%--<link href="${pageContext.request.contextPath}resources/css/weather.css" type="text/css" rel="stylesheet">--%>
        <title>Weather App</title>
        <!-- bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <!-- fontawesome -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- weather icons -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

</head>
<body>

<div class="hero container">
    <!-- navbar -->
    <nav class="navbar row">
        <a class="navbar-brand col-4" href="#">
            <strong>Weather Forecast</strong>
        </a>
        <form class="col-sm-12 col-md-7 form-inline" method="get" commandName="weather" id="forma">
            <div class="form-group col-8">
            <label for="city" class="sr-only">Password</label>
            <input class="form-control" type="text" name="city" id="city" placeholder="Search city">
            </div>
            <input class="btn btn-primary" type="submit" value="Show weather" />
        </form>
        <span id="date" class="lead"></span>
    </nav>
    <!-- weather section  -->
    <div class="container">
        <div class="row justify-content-around weather-container">
                <div class="col-xs-12 col-md-8 left-content ">
                    <img src="${cityPhoto}">
                    <div class="text-content ">
                        <h1 class="text-right">${cityName}</h1>
                        <div class="temperature">
                            <div class="text-right">
                                <span class="text-left">
                                    <div class="dmy">
                                        <div id="txt"></div>
                                        <div class="date">
                                            <!-- Date-JavaScript -->
                                            <script type="text/javascript">
                                            var mydate=new Date()
                                            var year=mydate.getYear()
                                            if(year<1000)
                                                year+=1900
                                            var day=mydate.getDay()
                                            var month=mydate.getMonth()
                                            var daym=mydate.getDate()
                                            if(daym<10)
                                                daym="0"+daym
                                            var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
                                            var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
                                            document.write(""+dayarray[day]+", "+montharray[month]+" "+daym+", "+year+"")
                                            </script>
                                            <!-- //Date-JavaScript -->
                                        </div>
                                    </div>
                                </span>
                                <div class="temp-right">${temperature}&#8451;</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 right-content">
                    <div class="text-center right-description">
                        <i id="icon-desc" class="wi owm-${weatherIcon}"></i>
                        <p class="desc">${description}</p>
                    </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(0)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(0)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(0)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(1)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(1)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(1)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(2)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(2)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(2)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(3)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(3)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(3)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(4)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(4)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(4)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(5)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(5)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(5)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(6)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(6)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(6)}&#8451;</p>
                        </div>
                        <div class="row cities justify-content-around">
                            <p class="bigCitydesc col-6">${hourForecast.get(7)}</p>
                            <i class="bigCityicon wi owm-${iconForecast.get(7)}"></i>
                            <p class="bigCitytemp">${temperatureForecast.get(7)}&#8451;</p>
                        </div>
                    </div>
            <div class="col-12 bottom-content">
                <div class="row">
                    <div class="humidity col-4 text-center">
                        <i class="wi wi-raindrop"></i><span> Humidity</span>
                        <div class="humidity">
                            ${humidity} %
                        </div>
                    </div>
                    <div class="wind col-4 text-center">
                        <i class="wi wi-strong-wind"></i><span> Wind Speed</span>
                        <div class="humidity">
                            ${wind} km/h
                        </div>
                    </div>
                    <div class="pressure col-4 text-center">
                        <i class="fa fa-eye"></i><span> Pressure</span>
                        <div class="humidity">
                            ${pressure} hPa
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="next-days text-center">
            <div class="text-center">
                <h2>Next days forecast</h2>
            </div>
            <div class="row next-days-cells">
                <div class="col-md-6">
                    <h2>Tommorow</h2>
                    <div class="description col-12 text-center">
                        <i id="tomorrowIcon-desc" class="wi owm-${nextWeatherIcon}"></i>
                        <p id="tomorrowDesc">${nextTemperature}&#8451;</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2>${nextNextDate}</h2>
                    <div class="description col-12 text-center">
                        <i id="tomorrowIcon-desc" class="wi owm-${nextNextWeatherIcon}"></i>
                        <p id="tomorrowDesc">${nextNextTemperature}&#8451;</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


        <script>
            function startTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('txt').innerHTML =
                    h + ":" + m + ":" + s;
                var t = setTimeout(startTime, 500);
            }
            function checkTime(i) {
                if (i < 10) {i = "0" + i}; // add zero in front of numbers < 10
                return i;
            }
        </script>
    </body>
</html>

